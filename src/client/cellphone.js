var myApp;
(function (myApp) {
    var cellphone = /** @class */ (function () {
        function cellphone($scope) {
            this.$scope = $scope;
        }
        cellphone.factory = function () {
            var CELLPHONE_REGEXR = /^09\d{8}$/;
            return function () {
                return {
                    require: 'ngModel',
                    link: function (scope, elm, attrs, ctrl) {
                        ctrl.$validators.cellphone = function (modelValue, viewValue) {
                            if (CELLPHONE_REGEXR.test(modelValue)) {
                                return true;
                            }
                            return false;
                        };
                    }
                };
            };
        };
        cellphone.$inject = ['$scope'];
        return cellphone;
    }());
    myApp.cellphone = cellphone;
    angular.module('myApp').directive('cellphone', cellphone.factory());
})(myApp || (myApp = {}));
//# sourceMappingURL=cellphone.js.map