declare var angular: ng.IAngularStatic;

module myApp {
  export class cellphone {
    static $inject = ['$scope'];

    constructor(private $scope: ng.IScope) {}

    public static factory () {
      const CELLPHONE_REGEXR = /^09\d{8}$/;

      return function () {
        return {
          require: 'ngModel',
          link: function(scope: ng.IScope, elm: ng.IAugmentedJQuery, attrs: ng.IAttributes, ctrl: ng.INgModelController) {
            ctrl.$validators.cellphone = function(modelValue: string, viewValue: string) {

              if (CELLPHONE_REGEXR.test(modelValue)) {
                return true;
              }

              return false;
            };
          }
        };
      }
    }
  }

  angular.module('myApp').directive('cellphone', cellphone.factory());
}
