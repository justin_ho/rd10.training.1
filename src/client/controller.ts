declare var angular: ng.IAngularStatic;

module myApp {
  export class myCtrl {
    static $inject = ['$scope'];

    public shippingTypeFormatName: object;
    public isSubmit: boolean;
    public user: object;
    public resultTable: object[];


    constructor(private $scope: ng.IScope) {
      this.shippingTypeFormatName = {
        2: '指定到貨日'
      };

      this.isSubmit = false;
      this.user = {};
      this.resultTable = [];
    }

    public submit () {
      this.isSubmit = true;

      if (this.$scope.recipient.$valid) {
        this.resultTable.push(JSON.parse(JSON.stringify(this.user)));
      }
    }

    public resetForm () {
      this.user = {};
    }
  }

  angular.module('myApp').controller('myCtrl', myCtrl);
}
