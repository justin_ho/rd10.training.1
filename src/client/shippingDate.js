var myApp;
(function (myApp) {
    var shippingDate = /** @class */ (function () {
        function shippingDate($scope) {
            this.$scope = $scope;
        }
        shippingDate.factory = function () {
            var SHIPPING_DATE_REGEXR = /^\d{4}\/(0[1-9]|1[0-2])\/(0[1-9]|[1-2][0-9]|3[0-1])$/;
            return function () {
                return {
                    require: 'ngModel',
                    link: function (scope, elm, attrs, ctrl) {
                        ctrl.$validators.shippingDate = function (modelValue, viewValue) {
                            if (SHIPPING_DATE_REGEXR.test(modelValue)) {
                                return true;
                            }
                            return false;
                        };
                    }
                };
            };
        };
        shippingDate.$inject = ['$scope'];
        return shippingDate;
    }());
    myApp.shippingDate = shippingDate;
    angular.module('myApp').directive('shippingDate', shippingDate.factory());
})(myApp || (myApp = {}));
//# sourceMappingURL=shippingDate.js.map