var myApp;
(function (myApp) {
    var myCtrl = /** @class */ (function () {
        function myCtrl($scope) {
            this.$scope = $scope;
            this.shippingTypeFormatName = {
                2: '指定到貨日'
            };
            this.isSubmit = false;
            this.user = {};
            this.resultTable = [];
        }
        myCtrl.prototype.submit = function () {
            this.isSubmit = true;
            if (this.$scope.recipient.$valid) {
                this.resultTable.push(JSON.parse(JSON.stringify(this.user)));
            }
        };
        myCtrl.prototype.resetForm = function () {
            this.user = {};
        };
        myCtrl.$inject = ['$scope'];
        return myCtrl;
    }());
    myApp.myCtrl = myCtrl;
    angular.module('myApp').controller('myCtrl', myCtrl);
})(myApp || (myApp = {}));
//# sourceMappingURL=controller.js.map