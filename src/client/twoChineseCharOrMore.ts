declare var angular: ng.IAngularStatic;

module myApp {
  export class twoChineseCharOrMore {
    static $inject = ['$scope'];

    constructor(private $scope: ng.IScope) {}

    public static factory () {
      const TWO_CHINESE_CHAR_REGEXR = /\w*[\u4e00-\u9fa5]\w*[\u4e00-\u9fa5]/;

      return function () {
        return {
          require: 'ngModel',
          link: function(scope: ng.IScope, elm: ng.IAugmentedJQuery, attrs: ng.IAttributes, ctrl: ng.INgModelController) {
            ctrl.$validators.twoChineseCharOrMore = function(modelValue: string, viewValue: string) {

              if (TWO_CHINESE_CHAR_REGEXR.test(modelValue)) {
                return true;
              }

              return false;
            };
          }
        };
      }
    }
  }

  angular.module('myApp').directive('twoChineseCharOrMore', twoChineseCharOrMore.factory());
}
