var myApp;
(function (myApp) {
    var twoChineseCharOrMore = /** @class */ (function () {
        function twoChineseCharOrMore($scope) {
            this.$scope = $scope;
        }
        twoChineseCharOrMore.factory = function () {
            var TWO_CHINESE_CHAR_REGEXR = /\w*[\u4e00-\u9fa5]\w*[\u4e00-\u9fa5]/;
            return function () {
                return {
                    require: 'ngModel',
                    link: function (scope, elm, attrs, ctrl) {
                        ctrl.$validators.twoChineseCharOrMore = function (modelValue, viewValue) {
                            if (TWO_CHINESE_CHAR_REGEXR.test(modelValue)) {
                                return true;
                            }
                            return false;
                        };
                    }
                };
            };
        };
        twoChineseCharOrMore.$inject = ['$scope'];
        return twoChineseCharOrMore;
    }());
    myApp.twoChineseCharOrMore = twoChineseCharOrMore;
    angular.module('myApp').directive('twoChineseCharOrMore', twoChineseCharOrMore.factory());
})(myApp || (myApp = {}));
//# sourceMappingURL=twoChineseCharOrMore.js.map