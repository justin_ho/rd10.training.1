declare var angular: ng.IAngularStatic;

module myApp {
  export class shippingDate {
    static $inject = ['$scope'];

    constructor(private $scope: ng.IScope) {}

    public static factory () {
      const SHIPPING_DATE_REGEXR = /^\d{4}\/(0[1-9]|1[0-2])\/(0[1-9]|[1-2][0-9]|3[0-1])$/;

      return function () {
        return {
          require: 'ngModel',
          link: function(scope: ng.IScope, elm: ng.IAugmentedJQuery, attrs: ng.IAttributes, ctrl: ng.INgModelController) {
            ctrl.$validators.shippingDate = function(modelValue: string, viewValue: string) {

              if (SHIPPING_DATE_REGEXR.test(modelValue)) {
                return true;
              }

              return false;
            };
          }
        };
      }
    }
  }

  angular.module('myApp').directive('shippingDate', shippingDate.factory());
}
